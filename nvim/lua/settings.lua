vim.wo.number = true
vim.wo.relativenumber = true
vim.cmd("syntax on")

vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4
vim.opt.expandtab = true
