require('lualine').setup {
    options = {
        theme = 'modus-vivendi',
        section_separators = { left = '', right = '' },
        component_separators = ''
    }
}
