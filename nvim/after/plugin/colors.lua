vim.cmd.colorscheme("sonokai")
vim.g.material_style = "darker"
vim.cmd('hi Normal guibg=None')
vim.cmd('hi Normal ctermbg=None')
vim.cmd("set termguicolors")
