local packer = require('packer')
local use = packer.use
packer.startup()

require('plugins')
require('keymap')
require('settings')
require('neovide')
